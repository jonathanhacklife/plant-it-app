# Plant It App

Una extensión del proyecto PlantIT el cual monitorea la salud de las plantas y las representa en emociones como emojis.

## Descripción:

Quienes viven en departamentos o pasan poco tiempo en sus hogares saben lo difícil que es mantener las plantas hidratadas y soleadas. **Esta maceta inteligente ayuda a los distraídos a conocer el ánimo de su planta a través de una simpática cara desde su App.**

Así, por ejemplo, si un cáctus necesita sol, la cara que la maceta tiene en su frente, mostrará un lado sombrío. Si en cambio, **se observa un rostro agitado y con la lengua afuera, es porque la planta necesita agua.**


## Especificaciones:
### ¿Para qué sirve?

Esta tecnología mide la temperatura del ambiente, la exposición a la luz y el estado de la tierra, y a partir de esto indica si la planta está enferma, fría o cansada. **Plant-It está diseñada para convertir tu planta en una mascota virtual. Usando sensores, esta maceta inteligente muestra 15 expresiones animadas diferentes.**

La pantalla tiene un sensor de movimiento para hacer mucho más viva la expresión de la planta, y sus ojos siguen a las personas que pasan por delante.

![https://hackster.imgix.net/uploads/attachments/995166/iphone-x-lua-app5d41fcbceda26.gif](https://hackster.imgix.net/uploads/attachments/995166/iphone-x-lua-app5d41fcbceda26.gif)

### Materiales:
- Arduino Nano R3
- Clavos galvanizados
- Soldador y estaño
- Cables

### Links y recursos:
[Artificial Plant Emotion Expressor (A.P.E.X.)](https://www.youtube.com/watch?v=BdaTOp6ieVo)

[IoT Emoji Sign](https://www.instructables.com/id/IoT-Emoji-Sign/)
